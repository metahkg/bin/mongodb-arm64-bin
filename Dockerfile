# some lines from: https://github.com/docker-library/mongo/blob/master/6.0/Dockerfile
# license: apache-2.0

FROM debian:bookworm

RUN groupadd --gid 999 --system mongodb && \
    useradd --uid 999 --system --gid mongodb --home-dir /data/db mongodb && \
    mkdir -p /data/db /data/configdb && \
    chown -R mongodb:mongodb /data/db /data/configdb

RUN apt-get update && \
    apt-get install -y --no-install-recommends ca-certificates dirmngr gnupg jq numactl procps && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

ENV GOSU_VERSION=1.12
ENV JSYAML_VERSION=3.13.1

# install gosu and jsyaml
RUN apt-get update && \
    apt-get install -y --no-install-recommends wget && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/ && \
    dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
    wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" && \
    wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc" && \
    export GNUPGHOME="$(mktemp -d)" && \
    gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && \
    gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu && \
    gpgconf --kill all && \
    rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc && \
    wget -O /js-yaml.js "https://github.com/nodeca/js-yaml/raw/${JSYAML_VERSION}/dist/js-yaml.js" && \
    apt-get remove wget -y && \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false && \
    chmod +x /usr/local/bin/gosu && \
    gosu --version && \
    gosu nobody true

ARG VERSION
ENV VERSION ${VERSION:-6.0}

ARG COMMIT_HASH
ENV COMMIT_HASH ${COMMIT_HASH:-master}

# install mongodb
RUN export unstable=$(bash -c "if [[ $VERSION != *.0  ]]; then echo '-unstable'; else echo ''; fi;") && \
    mkdir ~/.gnupg && \
    chmod 600 ~/.gnupg && \
    gpg --no-default-keyring --keyring /etc/apt/keyrings/wcyat.gpg --keyserver keyserver.ubuntu.com --recv-keys 968AEE35DEBD51DB3769CD0FB10D363E72255CCA && \
    echo "deb [signed-by=/etc/apt/keyrings/wcyat.gpg] https://gitlab.com/metahkg/bin/mongodb-arm64-bin/-/raw/${COMMIT_HASH}/repo/apt/debian bookworm/mongodb-org/${VERSION} main" >> /etc/apt/sources.list.d/mongodb.list && \
    apt-get update && \
    apt-get install mongodb-org${unstable}-server mongodb-org${unstable}-mongos mongodb-mongosh mongodb-database-tools -y && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

RUN ln -s /usr/bin/mongosh /usr/bin/mongo

RUN mkdir /docker-entrypoint-initdb.d

VOLUME /data/db /data/configdb

ENV HOME=/data/db

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 27017

CMD ["mongod"]
