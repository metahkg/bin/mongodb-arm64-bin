#!/bin/bash
docker run -it --rm -v $PWD:/app wcyat/apt-generate bash -c "cd /app && rm packages*.db && apt-ftparchive generate -c=aptftp.conf aptgenerate.conf"
docker run -it --rm -v $PWD:/app wcyat/apt-generate bash -c "cd /app && apt-ftparchive release -c=aptftp.conf dists/bookworm/mongodb-org/6.0 >dists/bookworm/mongodb-org/6.0/Release"
docker run -it --rm -v $PWD:/app wcyat/apt-generate bash -c "cd /app && apt-ftparchive release -c=aptftp.conf dists/bookworm/mongodb-org/6.2 >dists/bookworm/mongodb-org/6.2/Release"
gpg -u 968AEE35DEBD51DB3769CD0FB10D363E72255CCA -bao dists/bookworm/mongodb-org/6.0/Release.gpg dists/bookworm/mongodb-org/6.0/Release
gpg -u 968AEE35DEBD51DB3769CD0FB10D363E72255CCA -bao dists/bookworm/mongodb-org/6.2/Release.gpg dists/bookworm/mongodb-org/6.2/Release
gpg -u 968AEE35DEBD51DB3769CD0FB10D363E72255CCA --clear-sign --output dists/bookworm/mongodb-org/6.0/InRelease dists/bookworm/mongodb-org/6.0/Release
gpg -u 968AEE35DEBD51DB3769CD0FB10D363E72255CCA --clear-sign --output dists/bookworm/mongodb-org/6.2/InRelease dists/bookworm/mongodb-org/6.2/Release

