#!/bin/sh
docker run -v $PWD:/app --rm debian:bookworm bash -c """apt-get update && apt-get install gnupg ca-certificates -y && mkdir /root/.gnupg && chmod 600 /root/.gnupg && \
gpg --no-default-keyring --keyring /etc/apt/keyrings/wcyat.gpg --keyserver keyserver.ubuntu.com --recv-keys 968AEE35DEBD51DB3769CD0FB10D363E72255CCA && \
dpkg --add-architecture arm64 && \
echo 'deb [signed-by=/etc/apt/keyrings/wcyat.gpg] file:///app/apt/debian bookworm/mongodb-org/6.0 main' | tee /etc/apt/sources.list.d/mongodb-6.0.list && \
apt-get update && \
apt-get install mongodb-org-server mongodb-org-mongos mongodb-database-tools mongodb-mongosh -y && \
rm /etc/apt/sources.list.d/mongodb-6.0.list && \
echo 'deb [signed-by=/etc/apt/keyrings/wcyat.gpg] file:///app/apt/debian bookworm/mongodb-org/6.2 main' | tee /etc/apt/sources.list.d/mongodb-6.2.list && \
apt-get remove mongodb-org-server mongodb-org-mongos mongodb-database-tools mongodb-mongosh -y && \
apt-get update && \
apt-get install mongodb-org-unstable-server mongodb-org-unstable-mongos mongodb-database-tools mongodb-mongosh -y"""
