# MongoDB ARM64

MongoDB binaries compiled from source, specifically made for raspberry pi 4 (cortex a72).

## Why

The official mongodb arm64 binary is compiled for armv8.2-a, which is not compatible with the cpu (cortex a72) used by raspberry pi, armv8-a.

## Content

- mongod 6.0.4, 6.2.0
- mongos 6.0.4, 6.2.0
- mongosh 1.6.2 (slightly modified to remove segment analytics, see [metahkg/forks/mongosh@0735a88c](https://gitlab.com/metahkg/forks/mongosh/-/commit/387b68f276f6eab3c5e2e5876ad15c2a8be2ee37))
- mongodb database tools 100.6.0, 100.6.1

## Build parameters (for mongod and mongos)

```bash
TARGET_ARCH="aarch64"
CCFLAGS="-march=armv8-a+crc -mtune=cortex-a72"
```

## Versions

- 6.0.4 (compiled with gcc 10)
- 6.2.0 (compiled with gcc 12)

## Systems

Compatible with debian 12 bookworm or later. \
Tested on debian 12 (bookworm) and debian 13 (trixie). \
NOT compatible with debian 11 (bullseye) or ubuntu 22.04 (jammy), due to openssl3. \
Untested on other systems.

Can run (and tested) on raspberry pi 4 (with debian bookworm).

## Architecture

- armv8-a or later

## Dependencies (mongod and mongos)

- libc6 (>= 2.35)
- libcurl4 (>= 7.16.2)
- libssl3 (>= 3.0.0)
- libstdc++6 (>= 12)
- tzdata

## Docker image

The docker image is similar to the official [mongo](https://hub.docker.com/_/mongo) docker image, and can be used as a drop-in replacement. \
You can simply replace the mongo docker image in your command / docker-compose.yml with one of the following images:

> **_WARNING_**: in this docker image mongo is an alias of mongosh

### 6.0

```text
registry.gitlab.com/metahkg/bin/mongodb-arm64-bin:6.0
```

### 6.2

```text
registry.gitlab.com/metahkg/bin/mongodb-arm64-bin:6.2
```

## Install

### Apt

#### 6.0.4

```bash
sudo gpg --no-default-keyring --keyring /etc/apt/keyrings/wcyat.gpg --keyserver keyserver.ubuntu.com --recv-keys 968AEE35DEBD51DB3769CD0FB10D363E72255CCA
echo "deb [signed-by=/etc/apt/keyrings/wcyat.gpg] https://gitlab.com/metahkg/bin/mongodb-arm64-bin/-/raw/master/repo/apt/debian bookworm/mongodb-org/6.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-6.0.list
sudo apt update
sudo apt install mongodb-org-server mongodb-org-mongos mongodb-database-tools mongodb-mongosh -y
```

#### 6.2.0

```bash
sudo gpg --no-default-keyring --keyring /etc/apt/keyrings/wcyat.gpg --keyserver keyserver.ubuntu.com --recv-keys 968AEE35DEBD51DB3769CD0FB10D363E72255CCA
echo "deb [signed-by=/etc/apt/keyrings/wcyat.gpg] https://gitlab.com/metahkg/bin/mongodb-arm64-bin/-/raw/master/repo/apt/debian bookworm/mongodb-org/6.2 main" | sudo tee /etc/apt/sources.list.d/mongodb-6.2.list
sudo apt update
sudo apt install mongodb-org-unstable-server mongodb-org-unstable-mongos mongodb-database-tools mongodb-mongosh -y
```

### Directly install binaries (mongod and mongos)

> **_WARNING_**: You must satisfy the [dependencies](#Dependencies).

#### 6.0.4

```bash
sudo wget https://gitlab.com/metahkg/bin/mongodb-arm64-bin/-/raw/master/6.0.4/bin/mongod -O /usr/local/bin/mongod
sudo wget https://gitlab.com/metahkg/bin/mongodb-arm64-bin/-/raw/master/6.0.4/bin/mongos -O /usr/local/bin/mongos
```

#### 6.2.0

```bash
sudo wget https://gitlab.com/metahkg/bin/mongodb-arm64-bin/-/raw/master/6.2.0/bin/mongod -O /usr/local/bin/mongod
sudo wget https://gitlab.com/metahkg/bin/mongodb-arm64-bin/-/raw/master/6.2.0/bin/mongos -O /usr/local/bin/mongos
```
